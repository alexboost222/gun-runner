﻿using System;
using System.Collections.Generic;

namespace GunRunner
{
    public abstract class DisposableCollector : IDisposable
    {
        private readonly List<IDisposable> _disposables = new();

        public void Add(IDisposable disposable)
        {
            if (_disposables.Contains(disposable))
                _disposables.Add(disposable);
        }

        public void Dispose()
        {
            foreach (IDisposable disposable in _disposables)
            {
                disposable.Dispose();
            }
            _disposables.Clear();
        }
    }

    public static class DisposableExtensions
    {
        public static T AddTo<T>(this T disposable, DisposableCollector disposableCollector) where T : IDisposable
        {
            disposableCollector.Add(disposable);
            return disposable;
        }
    }
}