﻿using NaughtyAttributes;
using R3;
using UnityEngine;

namespace GunRunner
{
    public class HPContainer : MonoBehaviour
    {
        [SerializeField] private float startHP;

        public ReadOnlyReactiveProperty<float> HP => _hp;

        public void SetHP(float value) => _hp.Value = Mathf.Max(0, value);

        private ReactiveProperty<float> _hp;

        private void Awake()
        {
            _hp = new ReactiveProperty<float>(startHP).AddTo(this);
        }
    }
}