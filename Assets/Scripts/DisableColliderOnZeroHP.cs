﻿using R3;
using UnityEngine;

namespace GunRunner
{
    [RequireComponent(typeof(HPContainer))]
    public class DisableColliderOnZeroHP : MonoBehaviour
    {
        [SerializeField] private Collider collider;

        private void Start()
        {
            GetComponent<HPContainer>().HP.Where(hp => hp == 0f)
                .Subscribe(_ => collider.enabled = false).AddTo(this);
        }
    }
}