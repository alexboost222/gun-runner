﻿using R3;
using R3.Triggers;
using UnityEngine;

namespace GunRunner
{
    public class Wall : MonoBehaviour
    {
        private void Awake()
        {
            this.OnTriggerEnterAsObservable()
                .Select(triggeredCollider => triggeredCollider.GetComponentInParent<WallTrigger>())
                .Where(wallTrigger => wallTrigger != null)
                .Subscribe(wallTrigger => wallTrigger.Trigger()).AddTo(this);
        }
    }
}