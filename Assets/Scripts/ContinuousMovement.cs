using System;
using R3;
using UnityEngine;

namespace GunRunner
{
    public class ContinuousMovement : MonoBehaviour
    {
        [SerializeField] private float speed;

        private void Awake()
        {
            Observable.EveryUpdate().Subscribe(Run).AddTo(this);
        }

        private void Run(Unit _)
        {
            transform.position += transform.forward * (speed * Time.deltaTime);
        }
    }
}