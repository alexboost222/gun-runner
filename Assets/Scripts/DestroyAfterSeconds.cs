﻿using System;
using R3;
using UnityEngine;

namespace GunRunner
{
    public class DestroyAfterSeconds : MonoBehaviour
    {
        [SerializeField] private float secondsToDestroy;

        private void Awake()
        {
            Observable.Timer(TimeSpan.FromSeconds(secondsToDestroy))
                .Subscribe(_ => Destroy(gameObject)).AddTo(this);
        }
    }
}