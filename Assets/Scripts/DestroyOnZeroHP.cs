﻿using R3;
using UnityEngine;

namespace GunRunner
{
    [RequireComponent(typeof(HPContainer))]
    public class DestroyOnZeroHP : MonoBehaviour
    {
        private void Start()
        {
            GetComponent<HPContainer>().HP.Where(hp => hp == 0f)
                .Subscribe(_ => Destroy(gameObject)).AddTo(this);
        }
    }
}