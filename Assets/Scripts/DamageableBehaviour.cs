﻿using System;
using R3;
using UnityEngine;

namespace GunRunner
{
    public class DamageableBehaviour : MonoBehaviour, IDamageable
    {
        public Observable<float> DamageReceived => _damageReceived;

        public void DoDamage(float damage)
        {
            _damageReceived.OnNext(damage);
        }

        private void Awake()
        {
            _damageReceived.AddTo(this);
        }

        private readonly Subject<float> _damageReceived = new();
    }
}