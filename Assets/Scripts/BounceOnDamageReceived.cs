﻿using System.Threading;
using System.Threading.Tasks;
using LitMotion;
using R3;
using TNRD;
using UnityEngine;

namespace GunRunner
{
    public class BounceOnDamageReceived : MonoBehaviour
    {
        [SerializeField] private Vector3 startScale;
        [SerializeField] private Vector3 endScale;
        [SerializeField] private float duration;
        [SerializeField] private SerializableInterface<IDamageable> damageable;

        private void Awake()
        {
            damageable.Value.DamageReceived.SubscribeAwait(BounceAsync, AwaitOperation.Drop).AddTo(this);
        }

        private async ValueTask BounceAsync(float damage, CancellationToken ct)
        {
            await LMotion.Create(startScale, endScale, duration / 2f)
                .Bind(scale => transform.localScale = scale).AddTo(this);
            await LMotion.Create(endScale, startScale, duration / 2f)
                .Bind(scale => transform.localScale = scale).AddTo(this);
        }
    }
}