﻿using R3;

namespace GunRunner
{
    public interface IDamageable
    {
        Observable<float> DamageReceived { get; }
        void DoDamage(float damage);
    }
}