﻿using UnityEngine;
using UnityEngine.InputSystem;
using R3;

namespace GunRunner
{
    public class LineSwitcher : MonoBehaviour
    {
        [SerializeField] private float lineWidth;
        [SerializeField] private InputActionReference leftLineSwitch;
        [SerializeField] private InputActionReference rightLineSwitch;

        private int _currentLine;

        private void Awake()
        {
            leftLineSwitch.action.AsPerformedObservable().Subscribe(_ => SwitchLine(-1)).AddTo(this);
            rightLineSwitch.action.AsPerformedObservable().Subscribe(_ => SwitchLine(1)).AddTo(this);
        }

        private void SwitchLine(int lineNumberOffset)
        {
            int newLine = _currentLine + lineNumberOffset;
            if (newLine is < -1 or > 1)
                return;
            _currentLine = newLine;
            transform.position = new Vector3(_currentLine * lineWidth, transform.position.y, transform.position.z);
        }
    }
}