﻿using System;
using R3;
using UnityEngine;

namespace GunRunner
{
    public class Gun : MonoBehaviour
    {
        [SerializeField, Min(0.001f)] private float fireRate = 1f;
        [SerializeField] private GameObject bulletPrefab;

        private void Awake()
        {
            Observable.Interval(TimeSpan.FromSeconds(1 / fireRate)).Subscribe(Fire).AddTo(this);
        }

        private void Fire(Unit _)
        {
            Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        }
    }
}