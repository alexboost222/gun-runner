﻿using R3;
using UnityEngine;

namespace GunRunner
{
    [RequireComponent(typeof(WallTrigger))]
    public class DestroyOnWallTriggered : MonoBehaviour
    {
        private void Awake()
        {
            GetComponent<WallTrigger>().WallTriggered.Subscribe(_ => Destroy(gameObject)).AddTo(this);
        }
    }
}