﻿using System;
using R3;
using UnityEngine;

namespace GunRunner
{
    public class WallTrigger : MonoBehaviour
    {
        public Observable<Unit> WallTriggered => _wallTriggered;

        public void Trigger()
        {
            _wallTriggered.OnNext(Unit.Default);
        }

        private readonly Subject<Unit> _wallTriggered = new();

        private void Awake()
        {
            _wallTriggered.AddTo(this);
        }
    }
}