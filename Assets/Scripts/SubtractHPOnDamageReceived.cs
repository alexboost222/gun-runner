﻿using R3;
using UnityEngine;

namespace GunRunner
{
    [RequireComponent(typeof(HPContainer), typeof(IDamageable))]
    public class SubtractHPOnDamageReceived : MonoBehaviour
    {
        private HPContainer _hpContainer;

        private void Awake()
        {
            _hpContainer = GetComponent<HPContainer>();
            GetComponent<IDamageable>().DamageReceived
                .Subscribe(damage => _hpContainer.SetHP(_hpContainer.HP.CurrentValue - damage))
                .AddTo(this);
        }
    }
}