﻿using R3;
using R3.Triggers;
using UnityEngine;

namespace GunRunner
{
    public class DestroyOnTriggerEnter : MonoBehaviour
    {
        private void Awake()
        {
            this.OnTriggerEnterAsObservable().Subscribe(_ => Destroy()).AddTo(this);
        }

        private void Destroy()
        {
            Destroy(gameObject);
        }
    }
}