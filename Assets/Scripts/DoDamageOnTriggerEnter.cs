﻿using System;
using R3;
using R3.Triggers;
using UnityEngine;

namespace GunRunner
{
    public class DoDamageOnTriggerEnter : MonoBehaviour
    {
        [SerializeField] private float damage;

        private void Awake()
        {
            this.OnTriggerEnterAsObservable()
                .Select(triggeredCollider => triggeredCollider.GetComponentInParent<IDamageable>())
                .Where(damageable => damageable != null)
                .Subscribe(DoDamage);
        }

        private void DoDamage(IDamageable damageable) => damageable.DoDamage(damage);
    }
}